from abc import ABCMeta, abstractmethod


class IA(metaclass=ABCMeta):
    @staticmethod
    @abstractmethod
    def method_a():
        """An Abstract Method A"""


class ClassA(IA):
    def method_a(self):
        print("from class a")

class IB(metaclass=ABCMeta):
    @staticmethod
    @abstractmethod
    def method_b():
        """An Abstract Method B"""

class ClassB(IB):
    def method_b(self):
        print("from class b")

class Adapter(IA):
    def __init__(self):
        self.classb = ClassB()
    def method_a(self):
        "from class adapter"
        self.classb.method_b()

obj = Adapter()
obj.method_a()
